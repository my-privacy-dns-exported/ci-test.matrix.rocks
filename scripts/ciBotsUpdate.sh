#!/usr/bin/env bash

# Copyright by https://www.mypdns.org/p/Spirillen/
# License: https://www.mypdns.org/w/license/
# Issues: https://www.mypdns.org/maniphest/

# Get the latest IP-addresses from Travis-ci
ip=$(dig +short nat.travisci.net | sort)

# Clean out the old records
dnsdist -e "ciBots:clear()"
dnsdist -e "ciBots:size()"
for i in ${ip}
do
  dnsdist -e "ciBots:addMask('$i')"
done
dnsdist -e "ciBots:size()"


#dnsdist -e "ciBots:clear()"; dnsdist -e "ciBots:addMask('192.168.1.53')"


# This program is free software: you can redistribute it and/or modify
# it under the terms of the modified GNU Affero General Public License as
# published by the My Privacy DNS, either version 3 of the
# License, or any later version released at
# https://www.mypdns.org/w/License.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# modified GNU Affero General Public License for more details.

# You should have received a copy of the modified GNU Affero General Public License
# along with this program. If not, see https://www.mypdns.org/w/License.

# The modification: The standard AGPLv3 have been altered to NOT allow
# any to generate profit from our work. You are however free to use it to any
# NON PROFIT purpose. If you would like to use any of our code for profiteering
# YOU are obliged to contact https://www.mypdns.org/ for profit sharing.
